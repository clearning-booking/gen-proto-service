// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v4.24.2
// source: task-service.proto

package pb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// TaskServiceClient is the client API for TaskService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type TaskServiceClient interface {
	Sayhello(ctx context.Context, in *Ping, opts ...grpc.CallOption) (*Pong, error)
	CreateNewBooking(ctx context.Context, in *CreateNewBookingRequest, opts ...grpc.CallOption) (*CreateNewBookingResponse, error)
	CreateNewTaskType(ctx context.Context, in *TaskType, opts ...grpc.CallOption) (*CreateNewTaskTypeResponse, error)
	EstimatePrice(ctx context.Context, in *Booking, opts ...grpc.CallOption) (*GetPriceOfTaskResponse, error)
	UpdateAssignee(ctx context.Context, in *UpdateAssigneeRequest, opts ...grpc.CallOption) (*UpdateAssigneeResponse, error)
}

type taskServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewTaskServiceClient(cc grpc.ClientConnInterface) TaskServiceClient {
	return &taskServiceClient{cc}
}

func (c *taskServiceClient) Sayhello(ctx context.Context, in *Ping, opts ...grpc.CallOption) (*Pong, error) {
	out := new(Pong)
	err := c.cc.Invoke(ctx, "/taskservice.TaskService/Sayhello", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *taskServiceClient) CreateNewBooking(ctx context.Context, in *CreateNewBookingRequest, opts ...grpc.CallOption) (*CreateNewBookingResponse, error) {
	out := new(CreateNewBookingResponse)
	err := c.cc.Invoke(ctx, "/taskservice.TaskService/CreateNewBooking", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *taskServiceClient) CreateNewTaskType(ctx context.Context, in *TaskType, opts ...grpc.CallOption) (*CreateNewTaskTypeResponse, error) {
	out := new(CreateNewTaskTypeResponse)
	err := c.cc.Invoke(ctx, "/taskservice.TaskService/CreateNewTaskType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *taskServiceClient) EstimatePrice(ctx context.Context, in *Booking, opts ...grpc.CallOption) (*GetPriceOfTaskResponse, error) {
	out := new(GetPriceOfTaskResponse)
	err := c.cc.Invoke(ctx, "/taskservice.TaskService/EstimatePrice", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *taskServiceClient) UpdateAssignee(ctx context.Context, in *UpdateAssigneeRequest, opts ...grpc.CallOption) (*UpdateAssigneeResponse, error) {
	out := new(UpdateAssigneeResponse)
	err := c.cc.Invoke(ctx, "/taskservice.TaskService/UpdateAssignee", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// TaskServiceServer is the server API for TaskService service.
// All implementations must embed UnimplementedTaskServiceServer
// for forward compatibility
type TaskServiceServer interface {
	Sayhello(context.Context, *Ping) (*Pong, error)
	CreateNewBooking(context.Context, *CreateNewBookingRequest) (*CreateNewBookingResponse, error)
	CreateNewTaskType(context.Context, *TaskType) (*CreateNewTaskTypeResponse, error)
	EstimatePrice(context.Context, *Booking) (*GetPriceOfTaskResponse, error)
	UpdateAssignee(context.Context, *UpdateAssigneeRequest) (*UpdateAssigneeResponse, error)
	mustEmbedUnimplementedTaskServiceServer()
}

// UnimplementedTaskServiceServer must be embedded to have forward compatible implementations.
type UnimplementedTaskServiceServer struct {
}

func (UnimplementedTaskServiceServer) Sayhello(context.Context, *Ping) (*Pong, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Sayhello not implemented")
}
func (UnimplementedTaskServiceServer) CreateNewBooking(context.Context, *CreateNewBookingRequest) (*CreateNewBookingResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateNewBooking not implemented")
}
func (UnimplementedTaskServiceServer) CreateNewTaskType(context.Context, *TaskType) (*CreateNewTaskTypeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateNewTaskType not implemented")
}
func (UnimplementedTaskServiceServer) EstimatePrice(context.Context, *Booking) (*GetPriceOfTaskResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method EstimatePrice not implemented")
}
func (UnimplementedTaskServiceServer) UpdateAssignee(context.Context, *UpdateAssigneeRequest) (*UpdateAssigneeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateAssignee not implemented")
}
func (UnimplementedTaskServiceServer) mustEmbedUnimplementedTaskServiceServer() {}

// UnsafeTaskServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to TaskServiceServer will
// result in compilation errors.
type UnsafeTaskServiceServer interface {
	mustEmbedUnimplementedTaskServiceServer()
}

func RegisterTaskServiceServer(s grpc.ServiceRegistrar, srv TaskServiceServer) {
	s.RegisterService(&TaskService_ServiceDesc, srv)
}

func _TaskService_Sayhello_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Ping)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TaskServiceServer).Sayhello(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/taskservice.TaskService/Sayhello",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TaskServiceServer).Sayhello(ctx, req.(*Ping))
	}
	return interceptor(ctx, in, info, handler)
}

func _TaskService_CreateNewBooking_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateNewBookingRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TaskServiceServer).CreateNewBooking(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/taskservice.TaskService/CreateNewBooking",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TaskServiceServer).CreateNewBooking(ctx, req.(*CreateNewBookingRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TaskService_CreateNewTaskType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TaskType)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TaskServiceServer).CreateNewTaskType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/taskservice.TaskService/CreateNewTaskType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TaskServiceServer).CreateNewTaskType(ctx, req.(*TaskType))
	}
	return interceptor(ctx, in, info, handler)
}

func _TaskService_EstimatePrice_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Booking)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TaskServiceServer).EstimatePrice(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/taskservice.TaskService/EstimatePrice",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TaskServiceServer).EstimatePrice(ctx, req.(*Booking))
	}
	return interceptor(ctx, in, info, handler)
}

func _TaskService_UpdateAssignee_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateAssigneeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TaskServiceServer).UpdateAssignee(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/taskservice.TaskService/UpdateAssignee",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TaskServiceServer).UpdateAssignee(ctx, req.(*UpdateAssigneeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// TaskService_ServiceDesc is the grpc.ServiceDesc for TaskService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var TaskService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "taskservice.TaskService",
	HandlerType: (*TaskServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Sayhello",
			Handler:    _TaskService_Sayhello_Handler,
		},
		{
			MethodName: "CreateNewBooking",
			Handler:    _TaskService_CreateNewBooking_Handler,
		},
		{
			MethodName: "CreateNewTaskType",
			Handler:    _TaskService_CreateNewTaskType_Handler,
		},
		{
			MethodName: "EstimatePrice",
			Handler:    _TaskService_EstimatePrice_Handler,
		},
		{
			MethodName: "UpdateAssignee",
			Handler:    _TaskService_UpdateAssignee_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "task-service.proto",
}
